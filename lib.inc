section .text

%define EXIT 60
%define STDIN 0
%define READ 0
%define STDOUT 1
%define WRITE 1
%define SPACE 0x20
%define TAB 0x9
%define NEWLINE 0xA
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT
    xor rdi, rdi
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .length:
        cmp byte [rdi + rax], 0
        je .end
        inc rax
        jmp .length
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rax, WRITE
    mov rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, NEWLINE

; Принимает код символа и выводит его в stdout
print_char:
    mov rax, WRITE
    push rdi
    mov rdi, STDOUT
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jns print_uint
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r10, 10
    xor rax, rax
    xor rcx, rcx
    dec rsp
    mov [rsp], byte 0
    mov rax, rdi
    .to_decimal:
	xor rdx, rdx
	div r10
	add dl, '0'
	dec rsp
	mov [rsp], dl
	inc rcx
	test rax, rax
	jnz .to_decimal
    mov rdi, rsp
    inc rcx
    push rcx
    call print_string
    pop rcx
    add rsp, rcx
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    push rdi
    push rsi
    call string_length
    mov r10, rax
    mov rdi, rsi
    call string_length
    mov r9, rax
    pop rsi
    pop rdi
    cmp r10, r9
    jne .different
    xor rcx, rcx
    xor r10, r10
    .compare:
	mov r10b, byte [rdi + rcx]
        cmp r10b, byte [rsi + rcx] 
        jne .different
        cmp r10b, 0
        je .equal
        inc rcx
        jmp .compare
    .different:
        xor rax, rax
        ret
    .equal:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov rax, READ
    mov rdi, STDIN
    mov rsi, rsp
    mov rdx, 1
    syscall
    test rax, rax
    jz .end
    mov al, byte [rsp]
    inc rsp
    ret
    .end:
	xor rax, rax
	inc rsp
        ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    mov r8, rdi
    dec rsi
    mov r9, rsi
    xor r10, r10
    .check_spaces:
	call read_char
	cmp rax, SPACE
	je .check_spaces
	cmp rax, TAB
	je .check_spaces
	cmp rax, NEWLINE
	je .check_spaces
    .next_char:
	test rax, rax
	jz .success
	cmp rax, SPACE
	je .success
	cmp rax, TAB
	je .success
	cmp rax, NEWLINE
	je .success
	cmp r10, r9
	je .error
	mov [r8 + r10], rax
	inc r10
	call read_char
	jmp .next_char
    .success:
	mov [r8 + r10], byte 0
	mov rax, r8
	mov rdx, r10
	ret
    .error:
	xor rax, rax
	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    mov r10, 10
    xor r9, r9
    xor rdx, rdx
    .read_number:
	mov r9b, [rdi + rdx]
	cmp r9b, '9'
	ja .end
	cmp r9b, '0'
	jb .end
	sub r9b, '0'
	push rdx
	mul r10
	pop rdx
	add rax, r9
	inc rdx
	jmp .read_number
    .end:
	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte [rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    inc rax
    cmp rax, rdx
    ja .error
    xor rcx, rcx
    .copy_char:
	mov r10b, byte [rdi + rcx]
	mov byte [rsi + rcx], r10b
	cmp r10b, 0
	jz .success
	inc rcx
	jmp .copy_char
    .error:
	xor rax, rax
	ret
    .success:
    	dec rax
	ret
